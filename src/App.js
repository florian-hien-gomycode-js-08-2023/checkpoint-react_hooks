import MovieCard from "./Components/MovieCard";
import Filter from "./Components/Filter";
import MovieList from "./Components/MovieList";
import React, { useState, useEffect, useRef } from "react";
import "./App.css";

function App() {
  const [list, setList] = useState([]);

  const [titleFilter, setTitleFilter] = useState("");
  const [rateFilter, setRateFilter] = useState(null);

  const [films, setFilms] = useState([...MovieList]);
  console.log(films);

  function updateTitle(title) {
    // const filmsFiltrésByTitle = films.filter((film) =>
    //   film.title.toLowerCase().includes(title.toLowerCase().trim())
    // );
    // setList(filmsFiltrésByTitle);
    setTitleFilter(title);
  }
  function updateRate(rate) {
    //   const filmsFiltrésByRate = films.filter((film) =>
    //   rate ? (film.rating == rate) : (films)
    // );
    // setList(filmsFiltrésByRate);
    setRateFilter(Number(rate));
    console.log(Number(rate));
  }


  const addArticle = (newArticle) => {
    setFilms([...films, newArticle]);
  };


  useEffect(() => {
    setList(films);
  }, [films]);

  useEffect(() => {
    const filmsFiltrés = films.filter(
      (film) =>
        film.title.toLowerCase().includes(titleFilter.toLowerCase().trim()) && (rateFilter ? (Number(film.rating)=== rateFilter) : (true))
    );
    setList(filmsFiltrés);
  }, [titleFilter, rateFilter]);
  
  const h = window.innerHeight 
  const mW = {height:h-(h/14)}

  return (
    <div className="App">
      <div className="left">
        <Filter updateTitle={updateTitle} updateRate={updateRate} addArticle={addArticle}></Filter>
      </div>

      <div className="right" style={mW}>
        <div>
          <h2>Movies</h2>
        </div>
        <div className="movies" >
          <MovieCard list={list}></MovieCard>
        </div>
      </div>
    </div>
  );
}

export default App;
