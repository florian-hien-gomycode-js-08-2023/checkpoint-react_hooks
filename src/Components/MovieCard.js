import React from "react";

function MovieCard({list}) {
  return (
    <>
      {list.map((el,index) => (
        <div className="wrapper" key={index}>
          <div className="element-grow">
            <img
              src={el.posterURL}
              alt=""
              style={{ width: 230, height: 370, borderRadius:25 }}
            />
          </div>
          <div className="details">
            <p>
              <span>Title: </span>
              {el.title}
            </p>
            <p>
              <span>Description: </span>
              {el.description}
            </p>
            <p>
              <span>Rate: </span>
              {el.rating}
            </p>
          </div>
        </div>
      ))}
    </>
  );
}

export default MovieCard;
