import React, { useState, useEffect } from "react";

function Filter({ updateTitle, updateRate, addArticle }) {
  const [title, setTitle] = useState("");
  const [rate, setRate] = useState(0);

  const [addFilm, setAddFilm] = useState({
    title: "",
    description: "",
    posterURL: "",
    rating: "",
  });

  const handleAddFilmChange = (e) => {
    const { name, value } = e.target;
    setAddFilm({ ...addFilm, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (Object.values(addFilm).every((value) => value !== "")) {
      addArticle(addFilm);
      setAddFilm({
        posterURL: "",
        title: "",
        description: "",
        rating: "",
      });
    }
  };

  useEffect(() => {
    updateTitle(title);
    updateRate(rate);
  }, [title, rate]);

  return (
    <>
      <div className="filter">
        <h2>Filter</h2>
        <form action="">
          <input
            type="text"
            placeholder="Title"
            name="title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
          <input
            type="number"
            placeholder="Rate"
            min={0}
            max={10}
            name="rate"
            style={{ width: 60, fontSize: 17, textAlign: "center" }}
            value={rate}
            onChange={(e) => setRate(e.target.value)}
          />
        </form>
      </div>
      <div className="new-movie">
        <h2>Add Film</h2>
        <form onSubmit={handleSubmit}>
          <input
            type="url"
            name="posterURL"
            value={addFilm.posterURL}
            onChange={handleAddFilmChange}
            placeholder="Poster url"
          />
          <input
            type="text"
            name="title"
            value={addFilm.title}
            onChange={handleAddFilmChange}
            placeholder="Title"
          />
          <textarea
            type="text"
            name="description"
            value={addFilm.description}
            onChange={handleAddFilmChange}
            maxlength="100"
            placeholder="Description"
          ></textarea>
          <input
            type="number"
            name="rating"
            value={addFilm.rating}
            onChange={handleAddFilmChange}
            placeholder="Rating"
          />
          <button type="submit">Submit</button>
        </form>
      </div>
    </>
  );
}

export default Filter;
