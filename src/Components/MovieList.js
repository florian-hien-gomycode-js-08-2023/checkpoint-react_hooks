const MovieList = [
  {
    title: "Darkest Mind",
    description:
      "In the near future, teenagers have been decimated by an unknown virus. The survivors.",
    posterURL:
      "https://m.media-amazon.com/images/M/MV5BMTUxOGE3OTUtM2I2My00YzE3LTg2NDktZTI3NjE4NDdjMGFiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg",
    rating: 10,
  },
  {
    title: "Doctor Sleep",
    description:
      "Irrevocably marked by the trauma he endured as a child at the Overlook, Dan Torrance.",
    posterURL:
      "https://m.media-amazon.com/images/M/MV5BYmY3NGJlYTItYmQ4OS00ZTEwLWIzODItMjMzNWU2MDE0NjZhXkEyXkFqcGdeQXVyMzQzMDA3MTI@._V1_.jpg",
    rating: 10,
  },
  {
    title: "Annabelle",
    description:
      "Annabelle is an American horror film directed by John R. Leonetti and released in 20.",
    posterURL:
      "https://i.pinimg.com/originals/80/bf/38/80bf38f8e7fa9d98ccd8e180b48c2340.jpg",
    rating: 9.5,
  },
];

export default MovieList;
